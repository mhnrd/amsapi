<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login',  [App\Http\Controllers\UserAccountController::class, 'login']);
Route::post('asset-info',  [App\Http\Controllers\AssetController::class, 'getAssetInfo']);
Route::post('check-asset',  [App\Http\Controllers\AssetController::class, 'checkAsset']);
Route::post('asset-list',  [App\Http\Controllers\AssetController::class, 'assetList']);
Route::get('address-list',  [App\Http\Controllers\AssetController::class, 'getAddressList']);
Route::post('asset-info-id',  [App\Http\Controllers\AssetController::class, 'getAssetInfoByAssetID']);
Route::post('select-option',  [App\Http\Controllers\AssetController::class, 'dropDownInfo']);
Route::post('update-general-info',  [App\Http\Controllers\AssetController::class, 'updateGeneralInfo']);

