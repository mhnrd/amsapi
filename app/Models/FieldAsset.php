<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AttributeDetail;
use App\Models\AssetClass;

class FieldAsset extends Model
{
    
    use HasFactory;
    protected $table = 'ams_db.dbo.tblFA';


    public function setup(){
        return $this->hasOne(AttributeDetail::class, 'AD_ID', 'FA_ItemID');
    }

    public function supplier(){
        return $this->hasOne(AttributeDetail::class, 'AD_ID', 'FA_SupplierID');
    }

    public function affiliates(){
        return $this->hasOne(AttributeDetail::class, 'AD_ID', 'FA_AffiliatesID');
    }

    public function location(){
        return $this->hasOne(AttributeDetail::class, 'AD_ID', 'FA_DepartmentID');
    }

    public function class(){
        return $this->hasOne(AssetClass::class, 'FACS_ID', 'FA_AssetClass');
    }


}
