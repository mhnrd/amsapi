<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AssetClass;
use App\Models\Supplier;
use App\Models\UserAccount;
use App\Models\FieldAsset;
use App\Models\AttributeDetail;

class AssetController extends Controller
{
    
    /**
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function getAssetInfo(Request $request){

        $assetInfo = FieldAsset::with('setup','supplier','affiliates','location','class')->where('FA_RFID', $request->id)->get();
        
        return $assetInfo->first();

    }

    /**
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function getAssetInfoByAssetID(Request $request){

        $assetInfo = FieldAsset::with('setup','supplier','affiliates','location','class')->where('FA_ID', $request->id)->get();
        
        return $assetInfo->first();

    }

    /**
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function assetList(Request $request){

        $type = $request->type;
        $search = $request->value;

        if($type == 'Location'){
            
            if($search == ''){
                $assetList = FieldAsset::with('location')->get();
            }else{
                $assetList = FieldAsset::with('location')->whereHas('location', function ($query) use ($search){
                    $query->where('AD_Desc', 'LIKE', '%'.$search.'%');
                })->get();
            }
            
        }

        if($type == 'Asset ID'){
            
            if($search == ''){
                $assetList = FieldAsset::with('location')->get();
            }else{
                $assetList = FieldAsset::with('location')->where('FA_ID', 'LIKE', '%'.$search.'%')->get();
            }
            
        }

         /*->orWhereHas('supplier', function ($query) use ($search){
                    $query->where('AD_Desc', 'LIKE', '%'.$search.'%');
                })->orWhereHas('affiliates', function ($query) use ($search){
                    $query->where('AD_Desc', 'LIKE', '%'.$search.'%');
                })->orWhereHas('location', function ($query) use ($search){
                    $query->where('AD_Desc', 'LIKE', '%'.$search.'%');
                })->orWhereHas('class', function ($query) use ($search){
                    $query->where('FACS_ID', 'LIKE', '%'.$search.'%');
                })->get();*/

        return $assetList;


    }

    /**
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function checkAsset(Request $request){

        $success = array();
        $warning = array();
        $error = array();
        
        $selectLocationID = AttributeDetail::where('AD_Desc', $request->location)->get()->first();

        $assetInfo = FieldAsset::whereIn('FA_RFID', $request->ids)->get();

        $assetLostInfo = FieldAsset::with('location')->where('FA_DepartmentID', $selectLocationID->AD_ID)->whereNotIn('FA_RFID', $request->ids)->get();
        
        foreach($assetInfo as $asset){

            if($asset->FA_DepartmentID == $selectLocationID->AD_ID){
                $success[] = $asset->FA_RFID;
            }else{
                $warning[] = $asset->FA_RFID;
            }

        }

        foreach($assetLostInfo as $lost){
            if($lost->FA_RFID != ""){
                $error[] = $lost->FA_RFID;
            }else{
                $error[] = $lost->FA_ID.' - NO RFID TAG YET!';
            }
           
        }

        $json_data = array(
            "success" => $success,  
            "warning" => $warning,  
            "error" => $error  
        );
 
        return response()->json($json_data); 

    }

    /**
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function getAddressList(Request $request){

        $addressList = array();

        $assetList = AttributeDetail::get('AD_Desc');

        foreach($assetList as $location){

            $addressList[] = $location->AD_Desc;

        }

        return $addressList;

    }

    /** 
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function dropDownInfo(Request $request){

        $option = array();

        if($request->type == 'item_setup'){
            $optionList = AttributeDetail::select('AD_Desc AS Option')->where('AD_FK_Code', 400001)->get();
        }

        if($request->type == 'affiliates'){
            $optionList = AttributeDetail::select('AD_Desc AS Option')->where('AD_FK_Code', 400004)->get();
        }

        if($request->type == 'supplier'){
            $optionList = AttributeDetail::select('AD_Desc AS Option')->where('AD_FK_Code', 400002)->get();
        }

        if($request->type == 'department'){
            $optionList = AttributeDetail::select('AD_Desc AS Option')->where('AD_FK_Code', 400003)->get();
        }

        if($request->type == 'class'){
            $optionList = AssetClass::select('FACS_ID AS Option')->get();
        }

        foreach($optionList as $options){

            $option[] = $options->Option;

        }
        
        return $option;

    }


    /**
    * Transaction list
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function updateGeneralInfo(Request $request){

        $selectIfRfidUsed = FieldAsset::where('FA_RFID', $request->RFID)->get()->count();

        if($selectIfRfidUsed > 0){

            return true;
            
        }else{

            $fieldAssetUpdate = FieldAsset::where('FA_ID', $request->assetID)->update([
                'FA_RFID' => $request->RFID
            ]);
            return false;
        }
        

    }


}
